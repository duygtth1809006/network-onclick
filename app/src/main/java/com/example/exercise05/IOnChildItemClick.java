package com.example.exercise05;

public interface IOnChildItemClick {
    void onChildItemClick(int position);
}
