package com.example.exercise05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IOnChildItemClick {

    private ListView listView;
    private List<ContactModel> contactList = new ArrayList<>();
    private ContactAdapter contactAdapter;
    private ImageView imageView;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initView();

        contactAdapter = new ContactAdapter(contactList, this);
        contactAdapter.registerChildItemClick(this);
        listView.setAdapter(contactAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = contactList.get(position);
                Toast.makeText(MainActivity.this, contactModel.getName() + ": " + contactModel.getPhone(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        listView = findViewById(R.id.lvContact);
        textView = findViewById(R.id.tvName);
        imageView = findViewById(R.id.ivUser);
    }

    private void initData() {
        contactList.add(new ContactModel("Nguyen Van A", "09123123", R.drawable.iv_user_a));
        contactList.add(new ContactModel("Nguyen Van B", "09321321", R.drawable.iv_user_b));
        contactList.add(new ContactModel("Nguyen Van C", "09321321", R.drawable.iv_user_c));
        contactList.add(new ContactModel("Nguyen Van D", "09321321", R.drawable.iv_user_a));
        contactList.add(new ContactModel("Nguyen Van E", "09321321", R.drawable.iv_user_c));
        contactList.add(new ContactModel("Nguyen Van F", "09321321", R.drawable.iv_user_b));
        contactList.add(new ContactModel("Nguyen Van G", "09321321", R.drawable.iv_user_a));
        contactList.add(new ContactModel("Nguyen Van H", "098765432", R.drawable.iv_user_c));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        contactAdapter.unRegisterChildItemClick();
    }

    @Override
    public void onChildItemClick(int position) {
        ContactModel contactModel = contactList.get(position);
        imageView.setImageResource(contactModel.getImage());
        textView.setText(contactModel.getName());
    }
}