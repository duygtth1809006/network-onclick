package com.example.exercise05;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ContactAdapter extends BaseAdapter {

    private List<ContactModel> modelList;
    private Context context;
    private IOnChildItemClick iOnChildItemClick;

    public ContactAdapter(List<ContactModel> modelList, Context context) {
        this.modelList = modelList;
        this.context = context;
    }

    public void registerChildItemClick(IOnChildItemClick iOnChildItemClick) {
        this.iOnChildItemClick = iOnChildItemClick;
    }

    public void unRegisterChildItemClick() {
        this.iOnChildItemClick = null;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if(view == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            view = inflater.inflate(R.layout.activity_item, null);
            ViewHolder holder = new ViewHolder();
            holder.tvName = view.findViewById(R.id.tvName);
            holder.tvPhone = view.findViewById(R.id.tvPhone);
            holder.ivAvatar = view.findViewById(R.id.ivAvatar);
            holder.btCall = view.findViewById(R.id.btCall);
            holder.btEdit = view.findViewById(R.id.btEdit);
            view.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.tvName.setText(modelList.get(position).getName());
        holder.tvPhone.setText(modelList.get(position).getPhone());
        holder.ivAvatar.setImageResource(modelList.get(position).getImage());

        holder.btCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCall(position);
            }
        });

        holder.btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnChildItemClick.onChildItemClick(position);
            }
        });

        return view;
    }

    private void onCall(int position) {
        ContactModel model = modelList.get(position);
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + model.getPhone()));
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            //CALL
            return;
        }
        context.startActivity(intent);
    }

    static class ViewHolder {
        TextView tvName;
        TextView tvPhone;
        ImageView ivAvatar;
        ImageButton btCall;
        ImageButton btEdit;
    }
}
